
<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Edit Form
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Users </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Edit</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Edit User <a class="btn btn-sm btn-info" href="{{ route('users.index') }}">List</a>
        </div>
        <div class="card-body">

            <x-backend.layouts.elements.message :message="session('message')" />

            <x-backend.layouts.elements.errors :errors="$errors" />

            <form action="{{ route('users.profile.update') }}" method="post">
                @csrf
                @method('patch')

                <x-backend.form.input name="name" :value="$user->name" />
                <x-backend.form.input type="url" name="github_url" :value="$user->profile->github_url" />
                <x-backend.form.input type="url" name="facebook_url" :value="$user->profile->facebook_url" />
                <x-backend.form.input type="url" name="twitter" :value="$user->profile->twitter" />
                <x-backend.form.textarea name="biodata">
                {{ $user->profile->biodata }}
                </x-backend.form.textarea>

                <x-backend.form.button>Update</x-backend.form.button>

            </form>
        </div>
    </div>


</x-backend.layouts.master>