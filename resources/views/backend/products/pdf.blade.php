

<table id="datatablesSimple" border="1">
    <thead>
        <tr>
            <th>Sl#</th>
            <th>Title</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
    @php $sl=0 @endphp
        @foreach ($categories as $category)
        <tr>
            <td>{{ ++$sl }}</td>
            <td>{{ $category->title }}</td>
            <td>{{ $category->description }}</td>
            
        </tr>
        @endforeach

    </tbody>
</table>