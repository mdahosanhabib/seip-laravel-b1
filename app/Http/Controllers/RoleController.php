<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::latest()->get();

        return view('backend.roles.index', [
            'roles' => $roles
        ]);
    }

    public function show(Role $role)
    {
        // $users = User::where('role_id', $role->id)->get();
        return view('backend.roles.show', compact('role'));

    }
}
