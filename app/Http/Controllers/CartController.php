<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{
    public function add(Product $product)
    {
       
        $productIds = Cookie::get('productIds');

        if (!is_null($productIds)) {
            
            $productIds = unserialize($productIds);

            if (in_array($product->id, $productIds)) {
                return redirect()->back();;
            }

            array_push($productIds, $product->id);
           
            $data = serialize($productIds);
            
        } else {
            $data = serialize([$product->id]);
        }


        Cookie::queue('productIds', $data, 1000);

        return redirect()->back();
    }

    public function shoppingCart()
    {
        $productIds = Cookie::get('productIds');
        $data = unserialize($productIds);
    }
}
