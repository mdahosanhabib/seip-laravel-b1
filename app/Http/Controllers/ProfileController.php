<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        return view('backend.users.edit', compact('user'));
    }

    public function update(Request $request)
    {
        // auth()->user()->profile()->delete();

        $profileData = $request->except('name', '_token', '_method');
       
        auth()->user()->update(['name'=>$request->name]);

        auth()->user()->profile()->update($profileData);

        return redirect()->back()->withMessage('Profile Updated Successfully !');
    }
}
